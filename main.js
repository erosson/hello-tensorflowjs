// https://blog.yellowant.com/tensorflow-js-a-practical-guide-2ed58327c455
const xs = tf.tensor2d([[0,0], [0,1], [1,0], [1,1]])
console.log(xs+'')
const ys = tf.tensor2d([[0], [1], [1], [0]])
console.log(ys+'')

const model = tf.sequential()
model.add(tf.layers.dense({units: 8, inputShape: 2, activation: 'tanh'}))
model.add(tf.layers.dense({units: 1, activation: 'sigmoid'}))
model.compile({loss: 'binaryCrossentropy', optimizer: 'sgd', lr: 0.1})

document.addEventListener('DOMContentLoaded', () => {
  const progress = document.createElement('progress')
  progress.value = 0
  progress.max = 5000
  const plabel = document.createElement('div')
  plabel.innerText = "0/5000"
  const main = document.getElementById('main')
  main.innerHTML = ''
  main.appendChild(progress)
  main.appendChild(plabel)

  const started = Date.now()
  model.fit(xs, ys, {batchSize:1, epochs: 1000, callbacks: {onEpochEnd: n => {
    if (n%10 !== 0) return
    progress.value = n
    plabel.innerText = n+'/5000'
    console.log('epoch', n)
  }}}).then(() => {
    const out = model.predict(xs)
    console.log(out+'')
    document.getElementById('main').innerHTML = out
    const dur = Date.now() - started
    console.log(dur+'ms')
  })
})
